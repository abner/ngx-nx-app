# Angular 4/5 App Workspace criado com [`nrwl Nx`]()


Criando o projeto:

```bash
curl -fsSL https://raw.githubusercontent.com/nrwl/nx/master/packages/install/install-next.sh | bash -s ngx-nx-app

```


## Variações do projeto:

- Ionic 3 [URL DO BRANCH COM IONIC 3]

- Angular Material [URL DO BRANCH ANGULAR MATERIAL]

-  Com Stencil WebComponents [URL DO BRANCH COM STENCIL]


